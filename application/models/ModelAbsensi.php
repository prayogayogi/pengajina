<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelAbsensi extends CI_model
{
	public function getdata($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->from('data_pegawai');
		$result = $this->db->get();
		return $result;
	}

	public function getabsen($nik, $date)
	{
		$this->db->where('nik', $nik);
		$this->db->where('bulan', $date);
		$this->db->from('data_kehadiran');
		$result = $this->db->get();
		return $result;
	}

	public function get($nik, $date)
	{
		$this->db->where('nik', $nik);
		$this->db->where('bulan', $date);
		$this->db->from('data_kehadiran');
		$this->db->order_by('bulan', 'DESC');
		$result = $this->db->get();
		return $result;
	}

	public function store($data, $query, $date)
	{
		$this->db->set($data);
		$this->db->where('nik', $query);
		$this->db->where('bulan', $date);
		$this->db->update('data_kehadiran');
	}

	public function update($data, $nik, $date)
	{
		$this->db->set($data);
		$this->db->where('nik', $nik);
		$this->db->where('bulan', $date);
		$this->db->update('data_kehadiran');
	}

	public function storeAsosiasi($data)
	{
		$this->db->insert('data_kehadiran', $data);
	}

	public function validasi_bulan($nik, $date)
	{
		$this->db->where('nik', $nik);
		$this->db->where('bulan', $date);
		$this->db->from('data_kehadiran');
		$result = $this->db->get()->row_array();
		return $result;
	}
}
