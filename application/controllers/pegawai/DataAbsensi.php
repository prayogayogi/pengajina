<?php
defined('BASEPATH') or exit('No direct script access allowed');
class DataAbsensi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Load model
		$this->load->model("ModelAbsensi");

		// Cek Has akses
		if ($this->session->userdata('hak_akses') != '2') {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong>Anda Belum Golin!</strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>');
			redirect('welcome');
		}
	}

	public function index()
	{
		$data['title'] = "Data Absensi";
		$date = date("mY");
		$nik = $this->session->userdata('nik');
		$data['absensi'] = $this->ModelAbsensi->get($nik, $date)->result();
		$data['validasi_bulan'] = $this->ModelAbsensi->validasi_bulan($nik, $date);
		$this->load->view('templates_pegawai/header', $data);
		$this->load->view('templates_pegawai/sidebar');
		$this->load->view('pegawai/dataAbsensi', $data);
		$this->load->view('templates_pegawai/footer');
	}

	public function store()
	{
		$date = date("mY");
		$status_keadiran = $this->input->post("status_keadiran");

		$nik = $this->session->userdata('nik');
		$data_user = $this->ModelAbsensi->getabsen($nik, $date)->row_array();
		if ($status_keadiran == "hadir") {
			$result = [
				"hadir" => $data_user["hadir"] + 1
			];
		} elseif ($status_keadiran == "sakit") {
			$result = [
				"sakit" => $data_user["sakit"] + 1
			];
		} elseif ($status_keadiran == "alpha") {
			$result = [
				"alpha" => $data_user["alpha"] + 1
			];
		}
		$this->ModelAbsensi->store($result, $nik, $date);
		$this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
		<strong>Data berhasil ditambahkan! </strong>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>');
		redirect('pegawai/dataAbsensi/index');
	}

	public function storeAsosiasi()
	{
		$date = date("mY");
		$nik = $this->session->userdata('nik');
		$data_user = $this->ModelAbsensi->getdata($nik)->row_array();
		$data = [
			"bulan" => $date,
			"nik" => $nik,
			"nama_pegawai" => $data_user["nama_pegawai"],
			"jenis_kelamin" => $data_user["jenis_kelamin"],
			"nama_jabatan" => $data_user["jabatan"],
			"hadir" => 0,
			"sakit" => 0,
			"alpha" => 0
		];

		$this->ModelAbsensi->storeAsosiasi($data);
		$this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
		<strong>Absen Asosiasi bulan berikutnya berhasil ditambahkan! </strong>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>');
		redirect('pegawai/dataAbsensi/index');
	}
}
