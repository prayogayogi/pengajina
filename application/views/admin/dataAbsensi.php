<div class="container-fluid">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800"><?php echo $title ?></h1>
	</div>
	<div class="card mb-3">
		<div class="card-header bg-primary text-white">
			Filter Data Absensi Pegawai
		</div>
		<div class="card-body">
			<form class="form-inline">
				<div class="form-group mb-2">
					<label for="staticEmsil2">Bulan</label>
					<select class="form-control ml-3" name="bulan">
						<option value="">--Pilih Bulan--</option>
						<option value="01">Januari</option>
						<option value="02">Febuari</option>
						<option value="03">Maret</option>
						<option value="04">April</option>
						<option value="05">Mei</option>
						<option value="06">Juni</option>
						<option value="07">Juli</option>
						<option value="08">Agustus</option>
						<option value="09">September</option>
						<option value="10">Oktober</option>
						<option value="11">November</option>
						<option value="12">Desember</option>
					</select>
				</div>
				<div class="form-group mb-2 ml-5">
					<label for="staticEmsil2">Tahun</label>
					<select class="form-control ml-3" name="tahun">
						<option value="">--Pilih Tahun--</option>
						<?php $tahun = date('Y');
						for ($i = 2021; $i < $tahun + 10; $i++) { ?>
							<option value="<?php echo $i ?>"><?php echo $i ?></option>
						<?php } ?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary mb-2 ml-auto"><i class="fas fa-eye"></i> Tampilkan Data</button>
				<a href="<?php echo base_url('admin/dataAbsensi/inputAbsensi') ?>" class="btn btn-success mb-2 ml-3 d-none"><i class="fas fa-plus"></i>Input Kehadiran</a>
			</form>
		</div>

		<?php
		if ((isset($_GET['bulan']) && $_GET['bulan'] != '') && (isset($_GET['tahun']) && $_GET['tahun'] != '')) {
			$bulan = $_GET['bulan'];
			$tahun = $_GET['tahun'];
			$bulantahun = $bulan . $tahun;
		} else {
			$bulan = date('m');
			$tahun = date('Y');
			$bulantahun = $bulan . $tahun;
		}
		?>

		<div class="alert alert-info">
			Menampilkan Data Kehadiran Pegawai Bulan: <span class="font-weight-bold"><?php echo $bulan ?></span> Tahun:<span class="font-weight-bold"><?php echo $tahun ?></span>
		</div>
		<?php echo $this->session->flashdata('pesan') ?>
		<?php
		$jml_data = count($absensi);
		if ($jml_data > 0) {
		?>
			<table class="table table-bordered table-striped">
				<tr>
					<td class="text-center">No</td>
					<td class="text-center">NIK</td>
					<td class="text-center">Nama Pegawai</td>
					<td class="text-center">Jenis Kelamin</td>
					<td class="text-center">Jabatan</td>
					<td class="text-center">Hadir</td>
					<td class="text-center">Sakit</td>
					<td class="text-center">Alpha</td>
					<td class="text-center">Action</td>
				</tr>
				<?php $no = 1;
				foreach ($absensi as $a) : ?>
					<tr>
						<td><?php echo $no++ ?></td>
						<td><?php echo $a->nik ?></td>
						<td><?php echo $a->nama_pegawai ?></td>
						<td><?php echo $a->jenis_kelamin ?></td>
						<td><?php echo $a->nama_jabatan ?></td>
						<td><?php echo $a->hadir ?></td>
						<td><?php echo $a->sakit ?></td>
						<td><?php echo $a->alpha ?></td>
						<td>
							<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop<?= $a->id_kehadiran; ?>"><i class="fas fa-edit"></i></a>
						</td>
					</tr>
				<?php endforeach ?>
			</table>

		<?php } else { ?>
			<span class="badge badge-danger"><i class="fas fa-info-circle">Data masih kosong, Silahkan input data kehadiran pada bulan dan tahun yang Anda Pilih.</i></span>
		<?php } ?>
	</div>
</div>

<!-- Modal -->
<?php foreach ($absensi as $absen) : ?>
	<div class="modal fade" id="staticBackdrop<?= $absen->id_kehadiran; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">Modal ubah absensi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="<?= base_url('admin/dataAbsensi/update') ?>" method="post">
					<div class="modal-body">
						<input type="hidden" name="bulan" class="form-control" value="<?= $absen->bulan ?>">
						<input type="hidden" name="nik" class="form-control" value="<?= $absen->nik ?>">
						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" name="nama" id="nama" class="form-control" disabled value="<?= $absen->nama_pegawai ?>">
						</div>
						<div class="form-group">
							<label for="Hadir">Hadir</label>
							<input type="number" id="Hadir" name="hadir" class="form-control" value="<?= $absen->hadir ?>">
						</div>
						<div class="form-group">
							<label for="Sakit">Sakit</label>
							<input type="number" id="Sakit" name="sakit" class="form-control" value="<?= $absen->sakit ?>">
						</div>
						<div class="form-group">
							<label for="Alpha">Alpha</label>
							<input type="number" id="Alpha" name="alpha" class="form-control" value="<?= $absen->alpha ?>">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php endforeach; ?>
