<div class="container-fluid">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800"><?= $title ?></h1>
	</div>
	<div class="row">
		<div class="col col-10">
			<a class="btn btn-sm btn-success mb-3" href="#" data-toggle="modal" data-target="#staticBackdrop"><i class="fas fa-plus">Absensi Sekarang</i></a>
		</div>
		<?php
		if (empty($validasi_bulan)) { ?>
			<div class="col">
				<a href="#" class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#createtanggalbulanini"><i class="fas fa-plus"></i>Create Tanggal Bulan Ini</a>
			</div>
		<?php } ?>
	</div>
	<?php echo $this->session->flashdata('pesan') ?>
	<div class="card shadow mb-4">
		<div class="card-body">
			<div class="table-responsive">
				<table id="dataTable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Bulan</th>
							<th>Nik</th>
							<th>Jabatan</th>
							<th>Hadir</th>
							<th>Izin</th>
							<th>Alpha</th>
						</tr>
					</thead>
					<?php
					$no = 1;
					$hadir = 0;
					$sakit = 0;
					$alpha = 0;
					foreach ($absensi as $item) : ?>
						<tbody>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= date('F Y', strtotime($item->bulan)); ?></td>
								<td><?= $item->nik; ?></td>
								<td><?= $item->nama_jabatan; ?></td>
								<td><?= $item->hadir; ?></td>
								<td><?= $item->sakit; ?></td>
								<td><?= $item->alpha; ?></td>
							</tr>
							<?php
							$hadir += $item->hadir;
							$sakit += $item->sakit;
							$alpha += $item->alpha;
							?>
						<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="4" class="text-center">Jumlah</th>
								<th><?= $hadir; ?></th>
								<th><?= $sakit; ?></th>
								<th><?= $alpha; ?></th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="staticBackdropLabel">Absen Masuk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= base_url("pegawai/dataAbsensi/store") ?>" method="post">
				<div class="modal-body">
					<div class="form-group">
						<label for="tanggal">Tanggal</label>
						<input type="text" name="tanggal" class="form-control" disabled value="<?= date("d F Y") ?>" id="tanggal">
					</div>
					<div class="form-group">
						<label for="status_kehadiran">Status Kehadiran</label>
						<select class="form-control" name="status_keadiran" id="status_kehadiran">
							<option value="hadir">Masuk</option>
							<option value="sakit">Sakit</option>
							<option value="alpha">Alpha</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="createtanggalbulanini" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="createtanggalbulaniniLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="staticBackdropLabel">Create Absen Asosiasi bulan berikutnya</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= base_url("pegawai/dataAbsensi/storeAsosiasi") ?>" method="post">
				<div class="modal-body">
					<div class="form-group">
						<label for="tanggal">Periode</label>
						<input type="text" name="tanggal" class="form-control" disabled value="<?= date("F Y") ?>" id="tanggal">
					</div>
					<div class="form-group">
						<label for="nik">Nik</label>
						<input type="text" name="nik" class="form-control" disabled value="<?= $this->session->userdata('nik') ?>" id="nik">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
