<div class="container-fluid">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800"><?= $title ?></h1>
	</div>
	<table class="table table-striped table-bordered">
		<tr>
			<th>Bulan/Tahun</th>
			<th>Gaji Pokok</th>
			<th>Tj. Transportasi</th>
			<th>Uang Makan</th>
			<th>Potongan</th>
			<th>Total Gaji</th>
			<th>Cetek Selip</th>
		</tr>
		<?php foreach ($potongan as $potong) : ?>
			<?php $potongan = $potong->jml_potongan; ?>
		<?php endforeach; ?>

		<?php foreach ($gaji as $result) : ?>
			<?php $pot_gaji = $result->alpha * $potongan ?>
			<tr>
				<td><?= date('F Y', strtotime($result->bulan)); ?></td>
				<td>Rp. <?= number_format($result->gaji_pokok, 0, ',', '.') ?></td>
				<td>Rp. <?= number_format($result->tj_transport, 0, ',', '.') ?></td>
				<td>Rp. <?= number_format($result->uang_makan, 0, ',', '.') ?></td>
				<td>Rp. <?= number_format($pot_gaji, 0, ',', '.') ?></td>
				<td>Rp. <?= number_format($result->gaji_pokok + $result->tj_transport + $result->uang_makan - $pot_gaji, 0, ',', '.') ?></td>
				<td>
					<center>
						<a class=" btn btn-sm btn-primary" href="<?= base_url('pegawai/dataGaji/cetakSlip/' . $result->id_kehadiran) ?>"><i class="fa fa-print"></i></a>
					</center>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

</div>
