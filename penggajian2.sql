/*
SQLyog Community v13.1.9 (64 bit)
MySQL - 5.7.33 : Database - pengajian2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pengajian2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pengajian2`;

/*Table structure for table `data_jabatan` */

DROP TABLE IF EXISTS `data_jabatan`;

CREATE TABLE `data_jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(120) NOT NULL,
  `gaji_pokok` varchar(50) NOT NULL,
  `tj_transport` varchar(50) NOT NULL,
  `uang_makan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_jabatan` */

insert  into `data_jabatan`(`id_jabatan`,`nama_jabatan`,`gaji_pokok`,`tj_transport`,`uang_makan`) values 
(7,'Stap Manajer','5000000','20000000','50000'),
(14,'Derektor ','600000000','20000000','200000');

/*Table structure for table `data_kehadiran` */

DROP TABLE IF EXISTS `data_kehadiran`;

CREATE TABLE `data_kehadiran` (
  `id_kehadiran` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(15) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `hadir` int(11) NOT NULL,
  `sakit` int(11) NOT NULL,
  `alpha` int(11) NOT NULL,
  PRIMARY KEY (`id_kehadiran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_kehadiran` */

insert  into `data_kehadiran`(`id_kehadiran`,`bulan`,`nik`,`nama_pegawai`,`jenis_kelamin`,`nama_jabatan`,`hadir`,`sakit`,`alpha`) values 
(1,'102022','3525015201880002','Angga','laki-laki','stap marketing',0,0,0),
(2,'102022','3525015201880003','Admin','Laki-Laki','Derektor ',0,0,0),
(3,'102022','3525015201880004','Piter','Laki-Laki','Derektor ',25,1,0);

/*Table structure for table `data_pegawai` */

DROP TABLE IF EXISTS `data_pegawai`;

CREATE TABLE `data_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(30) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `setatus` varchar(20) NOT NULL,
  `photo` varchar(225) NOT NULL,
  `hak_akses` int(11) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pegawai` */

insert  into `data_pegawai`(`id_pegawai`,`nik`,`nama_pegawai`,`username`,`password`,`jenis_kelamin`,`jabatan`,`tanggal_masuk`,`setatus`,`photo`,`hak_akses`) values 
(12,'3525015201880002','Angga','angga','827ccb0eea8a706c4c34a16891f84e7b','Perempuan','Stap Manajer','2021-02-03','Pegawai Tetap','0987654321.jpg',2),
(19,'3525015201880003','Admin','admin','827ccb0eea8a706c4c34a16891f84e7b','Laki-Laki','Derektor ','2021-04-02','Pegawai Tetap','0987654321.jpg',1),
(26,'3525015201880004','Piter','piter','827ccb0eea8a706c4c34a16891f84e7b','Perempuan','Stap Manajer','2021-04-08','Pegawai Tetap','44743544576676452.jpg',2);

/*Table structure for table `hak_akses` */

DROP TABLE IF EXISTS `hak_akses`;

CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(50) NOT NULL,
  `hak_akses` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `hak_akses` */

insert  into `hak_akses`(`id`,`keterangan`,`hak_akses`) values 
(1,'admin',1),
(2,'pegawai',2);

/*Table structure for table `potongan_gaji` */

DROP TABLE IF EXISTS `potongan_gaji`;

CREATE TABLE `potongan_gaji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `potongan` varchar(120) NOT NULL,
  `jml_potongan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `potongan_gaji` */

insert  into `potongan_gaji`(`id`,`potongan`,`jml_potongan`) values 
(3,'alpha',20000);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
